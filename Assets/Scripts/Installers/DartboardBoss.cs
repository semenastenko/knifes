﻿using Knives.Environments;
using UnityEngine;

namespace Knives.Settings
{
    [CreateAssetMenu(fileName = "DartboardBoss", menuName = "SO/DartboardBoss")]
    public class DartboardBoss : ScriptableObject
    {
        [SerializeField] string bossName;
        [SerializeField] Material dartboardMaterial;
        [SerializeField] int knifeSpawnCount;
        [SerializeField] int stageKivesCount;
        [SerializeField] RotationRule[] rotationRules;

        public string BossName => bossName;
        public Material DartboardMaterial => dartboardMaterial;
        public int KnifeSpawnCount => knifeSpawnCount;
        public int StageKivesCount => stageKivesCount;
        public RotationRule[] RotationRules => rotationRules;
    }
}