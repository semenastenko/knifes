﻿using System;
using UnityEngine;

namespace Knives.Settings
{
    [CreateAssetMenu(fileName = "СurrencyOpenRequest", menuName = "SO/Requests/СurrencyOpenRequest")]
    public class СurrencyRequest : RequestSO
    {
        [SerializeField] Request request;

        public static Request CreateRequest() => new Request();

        public override BaseRequest GetRequest() => request;

        [Serializable]
        public class Request : BaseRequest
        {
            protected override bool Compare(BaseRequest request)
            {
               
                return false;
            }
        }
    }
}