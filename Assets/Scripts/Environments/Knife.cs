using Cysharp.Threading.Tasks;
using Knives.GameLogic;
using Knives.Settings;
using LerpUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Knives.Environments
{
    public class Knife : MonoBehaviour
    {
        public enum ThrowResult { none, hit, miss }

        private ParticlesController currentParticles;
        private Rigidbody2D _rigidbody;
        private PolygonCollider2D _collider;
        private SpriteRenderer _renderer;

        private ThrowResult throwResult;
        private float forcePower;

        private bool isThrown;
        private bool isAnimating;


        private void OnCreate()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _collider = GetComponent<PolygonCollider2D>();
            _renderer = GetComponent<SpriteRenderer>();
        }

        private void OnSpawn()
        {
            isThrown = false;
            _renderer.color = Color.white;
            _rigidbody.bodyType = RigidbodyType2D.Static;
            throwResult = ThrowResult.none;
            _collider.enabled = true;
        }

        public async void Animate(float delay)
        {
            isAnimating = true;
            await UniTask.WhenAll(
                Lerp.LerpValue(transform.position.y - 1, transform.position.y, delay)
                .OnNext(y => { transform.position = new Vector3(transform.position.x, y, transform.position.z); })
                .Task(),
                Lerp.LerpValue(0, 1, delay)
                .OnNext(a => { _renderer.color = new Color(1, 1, 1, a); })
                .Task()
                );
            isAnimating = false;
        }

        public UniTask Fade(float time)
        {
            return Lerp.LerpValue(1, 0, time, EaseType.BackOut)
                    .OnNext(a => { _renderer.color = new Color(1, 1, 1, a); })
                    .Task();
        }

        private void Cutomize(KnifeCustomization customization)
        {
            _renderer.sprite = customization.KnifeSprite;
            _collider.pathCount = customization.KnifeSprite.GetPhysicsShapeCount();

            List<Vector2> path = new List<Vector2>();
            for (int i = 0; i < _collider.pathCount; i++)
            {
                path.Clear();
                customization.KnifeSprite.GetPhysicsShape(i, path);
                _collider.SetPath(i, path.ToArray());
            }
        }

        public async UniTask<ThrowResult> Throw(CancellationTokenSource cancellationToken)
        {
            if (isThrown) return ThrowResult.none;
            await UniTask.WaitWhile(() => isAnimating, cancellationToken: cancellationToken.Token);
            isThrown = true;
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
            _rigidbody.AddForce(Vector2.up * forcePower);

            await UniTask.WaitWhile(() => throwResult == ThrowResult.none, cancellationToken: cancellationToken.Token);

            return throwResult;
        }

        private void Rebound(Vector2 force)
        {
            _collider.enabled = false;
            var torque = Random.Range(forcePower / 2, forcePower);
            _rigidbody.AddForce(Vector2.down * forcePower);
            _rigidbody.AddTorque(Mathf.Sign(force.x) * torque);
        }

        public void AddExplosionForce(float explosionForce)
        {
            transform.SetParent(null);
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
            _collider.enabled = false;

            var randomDir = transform.up + new Vector3(Random.Range(-1, 2), Random.Range(-1, 2));
            var torque = Random.Range(explosionForce / 2, explosionForce);
            _rigidbody.AddForce(randomDir * explosionForce);
            _rigidbody.AddTorque(Mathf.Sign(randomDir.x) * torque);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (throwResult != ThrowResult.none) return;
            var dartboard = collision.gameObject.GetComponent<Dartboard>();
            if (dartboard != null)
            {
                dartboard.Damage();
                _rigidbody.bodyType = RigidbodyType2D.Static;
                var point = Vector2.zero;
                foreach (var contact in collision.contacts)
                {
                    point += contact.point;
                }
                point /= collision.contactCount;
                transform.position = new Vector3(point.x, point.y, transform.position.z);
                currentParticles.InvokeChipsParticle(transform.position);
                transform.SetParent(dartboard.transform);
                throwResult = ThrowResult.hit;
            }
            else
            {
                currentParticles.InvokeBlindingEffect();
                Vector2 sum = Vector2.zero;
                foreach (var contact in collision.contacts)
                {
                    sum += contact.normal;
                }
                throwResult = ThrowResult.miss;
                Rebound(sum);
                currentParticles.InvokeCrashParticle(transform.position);
            }
        }

        public class Pool : MonoMemoryPool<Vector3, float, Knife>
        {
            [Inject] KnivesCustomizationController customizationController;
            [Inject] ParticlesController particles;
            private Vector3 startScale;
            private Quaternion startRotation;

            private void Customize(KnifeInfo knife)
            {
                foreach (var item in InactiveItems)
                {
                    item.Cutomize(knife.Cutomization);
                }
            }

            protected override void Reinitialize(Vector3 position, float power, Knife item)
            {
                item.transform.position = position;
                item.transform.localScale = startScale;
                item.transform.localRotation = startRotation;
                item.forcePower = power;
                item.currentParticles = particles;
            }

            protected override void OnCreated(Knife element)
            {
                startScale = element.transform.localScale;
                startRotation = element.transform.localRotation;
                base.OnCreated(element);
                element.OnCreate();

                customizationController.OnSelectKnife += Customize;
            }

            protected override void OnSpawned(Knife element)
            {
                base.OnSpawned(element);
                element.OnSpawn();
            }

            protected override void OnDespawned(Knife element)
            {
                base.OnDespawned(element);
            }
        }
    }
}
