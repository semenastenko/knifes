﻿namespace Knives.UI
{
    public interface ICounterableElement
    {
        public int Count { get; }
        void Increment();
        void Clear();
    }
}
