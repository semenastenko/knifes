﻿using Cysharp.Threading.Tasks;
using UICore;
using LerpUtility;
using UnityEngine;

namespace Knives.UI
{
    public class PausePanel : UIPanel
    {
        protected async override UniTask Closing()
        {
            await Lerp.LerpValue(transform.position.y, Screen.height * 1.5f, 0.2f, EaseType.Linear)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }
    }
}
