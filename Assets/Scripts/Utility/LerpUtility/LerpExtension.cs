﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

namespace LerpUtility
{
    public partial class LerpControl : IDisposable
    {
        public enum UpdateType { update, fixedUpdate, lateUpdate }

        public void EveryUpdate()
        {
            (bool, float) progress = (true, 0);
            dispose = Observable.EveryUpdate().Subscribe(_ =>
            {
                if (progress.Item1 && !isDispose)
                {
                    progress = controlDelegate(param.a, param.b, param.t, progress.Item2, onNext, onDone, param.ease);
                }
                else
                {
                    dispose?.Dispose();
                }
            });
        }

        public void EveryFixedUpdate()
        {
            (bool, float) progress = (true, 0);
            dispose = Observable.EveryFixedUpdate().Subscribe(_ =>
            {
                if (progress.Item1 && !isDispose)
                {
                    progress = controlDelegate(param.a, param.b, param.t, progress.Item2, onNext, onDone, param.ease);
                }
                else
                {
                    dispose?.Dispose();
                }
            });
        }

        public void EveryLateUpdate()
        {
            (bool, float) progress = (true, 0);
            dispose = Observable.EveryLateUpdate().Subscribe(_ =>
            {
                if (progress.Item1 && !isDispose)
                {
                    progress = controlDelegate(param.a, param.b, param.t, progress.Item2, onNext, onDone, param.ease);
                }
                else
                {
                    dispose?.Dispose();
                }
            });
        }

        public void Do(UpdateType update = UpdateType.update)
        {
            switch (update)
            {
                case UpdateType.update:
                    EveryUpdate();
                    break;
                case UpdateType.fixedUpdate:
                    EveryFixedUpdate();
                    break;
                case UpdateType.lateUpdate:
                    EveryLateUpdate();
                    break;
                default:
                    break;
            }
        }
    }
}
