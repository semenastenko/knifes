﻿using Knives.Settings;
using PolyAndCode.UI;
using System;
using UnityEngine;

namespace Knives.Environments
{
    public class KnifeInfo : IContractCell
    {
        private bool isSelect;
        private KnifeCustomization cutomization;
        private bool isOpen;

        public bool IsSelect => isSelect;
        public KnifeCustomization Cutomization => cutomization;
        public bool IsOpen => isOpen;

        public event Action<KnifeInfo, bool> OnSelect;
        public event Action<KnifeInfo> OnOpen;
        private Action<KnifeInfo, bool> selectHandler;

        public KnifeInfo(KnifeCustomization cutomization, bool isOpen, Action<KnifeInfo, bool> selectHandler)
        {
            this.cutomization = cutomization;
            this.isOpen = isOpen;
            this.selectHandler = selectHandler;
        }

        public void RemoveListeners()
        {
            OnSelect = null;
            OnOpen = null;
        }

        public void SetSelect(bool value)
        {
            isSelect = value;
            OnSelect?.Invoke(this, value);
            selectHandler?.Invoke(this, value);
        }

        public void Open()
        {

            Debug.Log($"OpenSYKA");
            isOpen = true;
            OnOpen?.Invoke(this);
        }

        Contract IContractCell.GetContract<Contract>()
        {
            return this as Contract;
        }
    }
}
