﻿using System.Collections.Generic;
using UnityEngine;

namespace UICore
{
    public class UIElementManager<Value> where Value : IUIElement
    {
        private Value[] allElements;

        protected IReadOnlyCollection<Value> AllElements => allElements;

        public static Manager Create<Manager, Element>(Element[] elements)
            where Manager : UIElementManager<Element>, new()
            where Element : IUIElement
        {
            var elementManager = new Manager();
            elementManager.Construct(elements);
            return elementManager;
        }

        private void Construct(Value[] elements)
        {
            allElements = elements;
            OnConstruct();
        }

        protected virtual void OnConstruct() { }

        public T GetElement<T>(System.Predicate<T> match) where T : class, Value
        {
            foreach (var element in AllElements)
            {
                if (element is T && match.Invoke(element as T))
                    return element as T;
            }
            return null;
        }

        public T[] GetAllElements<T>(System.Predicate<T> match) where T : class, Value
        {
            List<T> all = new List<T>();
            foreach (var element in AllElements)
            {
                if (element is T && match.Invoke(element as T))
                {
                    all.Add(element as T);
                }
            }
            return all.ToArray();
        }

        public Element GetElementInParent<Element>(Transform transform) where Element : class, IUIElement
        {
            Element panel = null;
            Transform parent = transform.parent;
            while (panel == null)
            {
                panel = parent.GetComponent<Element>();
                if (panel == null)
                {
                    parent = parent.parent;
                }

                if (parent == null || parent.GetComponent<Canvas>() != null)
                {
                    break;
                }
            }
            return panel;
        }
    }
}