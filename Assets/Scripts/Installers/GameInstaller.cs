using System.Collections;
using System.Collections.Generic;
using Zenject;
using Knives.UI;
using Knives.GameLogic;
using Knives.Environments;
using Knives.Settings;
using SaveCore;
using UnityEngine;

namespace Knives.Installers
{
    public class GameInstaller : MonoInstaller
    {
        private OnApplicationPauseEvent _onApplicationPauseEvent;

        [Inject] GameSettings _gameSettings;
        [Inject] EnvironmentsSettings _environmentsSettings;
        public override void InstallBindings()
        {
            Application.targetFrameRate = 60;
            Vibration.Init();

            _onApplicationPauseEvent = new OnApplicationPauseEvent();

            Container.BindInterfacesAndSelfTo<UIController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<InputController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GameController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<EnvironmentController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ParticlesController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<StageController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<KnivesCustomizationController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<RecordController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<CurrencyController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SettingsController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SaveLoadController>().AsSingle().NonLazy();
            
            Container.Bind<OnApplicationPauseEvent>().FromInstance(_onApplicationPauseEvent);

            Container.BindMemoryPool<Dartboard, Dartboard.Pool>()
                .WithInitialSize(_environmentsSettings.DartboardPoolCount)
                .FromComponentInNewPrefab(_environmentsSettings.DartboardPrefab)
                .UnderTransformGroup("PoolContainer - " + _environmentsSettings.DartboardPrefab.name);

            Container.BindMemoryPool<Knife, Knife.Pool>()
                .WithInitialSize(_environmentsSettings.KnifePoolCount)
                .FromComponentInNewPrefab(_environmentsSettings.KnifePrefab)
                .UnderTransformGroup("PoolContainer - " + _environmentsSettings.KnifePrefab.name);

            Container.BindMemoryPool<Apple, Apple.Pool>()
                .WithInitialSize(_environmentsSettings.ApplePoolCount)
                .FromComponentInNewPrefab(_environmentsSettings.ApplePrefab)
                .UnderTransformGroup("PoolContainer - " + _environmentsSettings.ApplePrefab.name);

            Container.BindMemoryPool<Indicator, Indicator.Pool>()
                .WithInitialSize(_environmentsSettings.IndicatorPoolCount)
                .FromComponentInNewPrefab(_environmentsSettings.IndicatorPrefab)
                .UnderTransformGroup("PoolContainer - " + _environmentsSettings.IndicatorPrefab.name);
        }

        private void OnApplicationPause(bool pause)
        {
            _onApplicationPauseEvent.Invoke(pause);

        }

#if UNITY_EDITOR
        private void OnApplicationQuit()
        {
            _onApplicationPauseEvent.Invoke(true);
        }
#endif
    }
}