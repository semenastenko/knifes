﻿using Knives.Environments;
using Knives.Settings;
using UnityEngine;

namespace Knives.GameLogic
{
    public class Stage
    {
        private int number;
        private DartboardBoss boss;
        private int spawnKnivesCount;
        private int knivesCount;
        private RotationRule[] rotationRules;
        private bool canAppleSpawn;

        public int Number => number;
        public DartboardBoss Boss => boss;
        public int  SpawnKnivesCount => spawnKnivesCount;
        public int StageKnivesCount => knivesCount;
        public RotationRule[] RotationRules => rotationRules;
        public bool CanAppleSpawn => canAppleSpawn;
        public bool IsBoss => (Number) % 5 == 0;
        public bool IsNextBoss => (Number + 1) % 5 == 0;

        public void Next(int spawnKnivesCount, int knivesCount, bool canAppleSpawn, RotationRule[] rotationRules, DartboardBoss boss = null)
        {
            number++;
            this.spawnKnivesCount = spawnKnivesCount;
            this.boss = boss;
            this.knivesCount = knivesCount;
            this.canAppleSpawn = canAppleSpawn;
            this.rotationRules = rotationRules;
        }

        public void Reset() => number = 0;
    }
}
