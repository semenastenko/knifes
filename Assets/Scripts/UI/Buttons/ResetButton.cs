﻿using System;
using Zenject;
using UICore;

namespace Knives.UI
{
    public class ResetButton : UIButton
    {
        [Inject] SaveCore.SaveLoadController saveController;
        protected override void Start()
        {
            try
            {
                onClick.AddListener(saveController.ClearData);
            }
            catch { }
        }

        protected override TimeSpan GetDelay() => TimeSpan.FromSeconds(0f);
    }
}
