using System.Collections;
using System.Collections.Generic;
using UICore;
using UnityEngine;
using Zenject;

namespace Knives.UI
{
    public class KinvesCounter : MonoBehaviour, ICounterableElement
    {
        [Inject] Indicator.Pool pool;

        private readonly List<Indicator> placedIndicators = new();
        private int currentIndicator;

        public int Count => currentIndicator;

        public void SetIndicators(int count)
        {
            ClearIndicators();
            for (int i = 0; i < count; i++)
            {
                placedIndicators.Add(pool.Spawn(transform));
            }
            currentIndicator = 0;
        }

        public bool SelectNextIndicator()
        {
            if (currentIndicator < placedIndicators.Count)
            {
                placedIndicators[currentIndicator].Select();
                currentIndicator++;
                if (currentIndicator == placedIndicators.Count)
                    return false;
                return true;
            }
            return false;
        }

        private void ClearIndicators()
        {
            while (placedIndicators.Count > 0)
            {
                var item = placedIndicators[0];
                placedIndicators.Remove(item);
                pool.Despawn(item);
            }
        }

        public void Increment()
        {
            SelectNextIndicator();
        }

        public void Clear()
        {
            ClearIndicators();
        }
    }
}
