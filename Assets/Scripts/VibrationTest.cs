using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationTest : MonoBehaviour
{
    public void Vibrate(int value)
    {
        switch (value)
        {
            case 0:
                Vibration.Vibrate();
                break;
            case 1:
                Vibration.VibratePop();
                break;
            case 2:
                Vibration.VibratePeek();
                break;
            case 3:
                Vibration.VibrateNope();
                break;
            case 4:
                Vibration.Vibrate(500);
                break;
            case 5:
                //Mortal Combat
                long[] pattern = {0, 100, 200, 100, 200, 100, 200, 100, 200, 100, 100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100, 100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 50, 50, 100, 800};
                Vibration.Vibrate(pattern, -1);
                break;
            case 6:
                //Dream Theater's Overture 1928
                long[] pattern1 = { 0, 75, 25, 75, 25, 75, 25, 75, 525, 75, 25, 75, 25, 75, 25, 75, 25, 75, 25, 75, 25, 75, 225, 75, 25, 75, 25, 75, 25, 75, 225, 75, 25, 75, 25, 75, 25, 75, 525, 75, 25, 75, 25, 75, 25, 75, 25, 75, 25, 75, 25, 75, 225, 75, 25, 75, 25, 75, 25, 75, 225 };
                Vibration.Vibrate(pattern1, -1);
                break;
            case 7:
                //Added Nocturne.
                long[] pattern2 = { 0, 660, 60, 180, 60, 60, 180, 60, 180, 60, 180, 420, 60, 180, 60, 60, 180, 60, 180, 60, 180, 420, 60, 180, 60, 60, 180, 60, 180, 60, 180, 420, 60, 180, 60, 60, 180, 60, 180, 420, 60, 420, 60 };
                Vibration.Vibrate(pattern2, -1);
                break;
            case 8:
                //Battlefield 4 Drum Rhythm
                long[] pattern3 = { 0, 175, 25, 100, 300, 100, 300, 100, 100, 100, 300, 200, 600 };
                Vibration.Vibrate(pattern3, -1);
                break;
            case 9:
                //Boney M - Rasputin
                long[] pattern4 = { 0, 200, 200, 200, 200, 100, 100, 100, 100, 100, 300, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 500, 100, 100, 100, 100, 100, 100, 100, 300, 200, 200, 100, 100, 100, 100, 100, 100, 100, 900 };
                Vibration.Vibrate(pattern4, -1);
                break;
            default:
                break;
        }
        
    }
}
