﻿using UnityEditor;
using UnityEditor.UI;

namespace Knives.UI.Editor
{
#if UNITY_EDITOR

    [CustomEditor(typeof(SwitchToggle))]
    public class MenuButtonEditor : ToggleEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onLable"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("offLable"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("transitionTime"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("transitionScale"));
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}
