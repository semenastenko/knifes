﻿using Knives.Settings;
using UnityEngine;
using Zenject;

namespace Knives.Installers
{
    [CreateAssetMenu(fileName = "SOInstaller", menuName = "SO/SOInstaller")]
    public class SOInstaller : ScriptableObjectInstaller<SOInstaller>
    {
        [SerializeField] GameSettings gameSettings;
        [SerializeField] EnvironmentsSettings environmentsSettings;

        public override void InstallBindings()
        {
            Container.BindInstances(gameSettings);
            Container.BindInstances(environmentsSettings);
        }
    }
}