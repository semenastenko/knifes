﻿using UnityEngine;

namespace Knives.Settings
{
    [CreateAssetMenu(fileName = "Dartboard", menuName = "SO/DartboardCustomization")]
    public class DartboardCustomization : ScriptableObject
    {
        [SerializeField] Material dartboardMaterial;

        public Material DartboardMaterial => dartboardMaterial;
    }
}