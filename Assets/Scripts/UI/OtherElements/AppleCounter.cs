﻿using LerpUtility;
using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class AppleCounter : MonoBehaviour, ICounterableElement
    {
        [SerializeField] Text text;
        [SerializeField] Image image;
        private int count = 0;

        public int Count => count;

        public void SetCount(int value)
        {
            count = value;
            text.text = count.ToString();
            Animate();
        }

        public void Increment()
        {
            count++;
            text.text = count.ToString();
        }

        public void Clear()
        {
            count = 0;
            text.text = count.ToString();
        }

        private async void Animate()
        {
            var startScale = 1;
            await Lerp.LerpValue(startScale * 1.5f, startScale, 0.3f, EaseType.BackIn)
                .OnNext(s => { image.transform.localScale = Vector3.one * s; })
                .Task();
        }
    }
}
