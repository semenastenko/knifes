﻿using Cysharp.Threading.Tasks;
using UICore;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Knives.UI
{
    public class SettingsButton : UIButton
    {
        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            transform.localScale = Vector3.one * 0.9f;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            transform.localScale = Vector3.one;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            transform.localScale = Vector3.one;
        }
    }
}
