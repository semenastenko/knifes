﻿using Cysharp.Threading.Tasks;
using UnityEngine;
using System;
using System.Threading;

namespace LerpUtility
{
    public delegate void LerpAction();
    public delegate void LerpAction<T>(T t);
    public delegate (bool a, float b) LerpControlDelegate(float a, float b, float t, float e, LerpAction<float> next, LerpAction done, EaseType ease);

    public partial class LerpControl : IDisposable
    {
        protected struct LerpParam
        {
            public float a;
            public float b;
            public float t;
            public EaseType ease;

            public LerpParam(float a, float b, float t, EaseType ease)
            {
                this.a = a;
                this.b = b;
                this.t = t;
                this.ease = ease;
            }
        }

        protected LerpControlDelegate controlDelegate;

        protected LerpAction onDone;
        protected LerpAction<float> onNext;

        protected LerpParam param;
        protected bool isDispose;
        protected IDisposable dispose;
        private LerpControl() { }

        public LerpControl(LerpControlDelegate controlDelegate, float a, float b, float t, EaseType ease)
        {
            this.controlDelegate = controlDelegate;
            param = new LerpParam(a, b, t, ease);
        }

        public async UniTask Task()
        {
            (bool, float) progress = (true, 0);
            while (progress.Item1 && !isDispose)
            {
                progress = controlDelegate(param.a, param.b, param.t, progress.Item2, onNext, onDone, param.ease);
                await UniTask.Yield();
            }
        }

        public async UniTask Task(CancellationTokenSource cancellationToken)
        {
            (bool, float) progress = (true, 0);
            while (progress.Item1 && !isDispose)
            {
                progress = controlDelegate(param.a, param.b, param.t, progress.Item2, onNext, onDone, param.ease);
                await UniTask.Yield(cancellationToken.Token);
            }
        }

        public LerpControl OnDone(LerpAction action)
        {
            onDone = action;
            return this;
        }

        public LerpControl OnNext(LerpAction<float> action)
        {
            onNext = action;
            return this;
        }

        public void Dispose()
        {
            isDispose = true;
            dispose?.Dispose();
            onDone = null;
            onNext = null;
        }
    }

    public enum EaseType
    {
        Linear,
        QuadraticIn, QuadraticOut, QuadraticInOut,
        CubicIn, CubicOut, CubicInOut,
        QuarticIn, QuarticOut, QuarticInOut,
        QuinticIn, QuinticOut, QuinticInOut,
        SinusoidalIn, SinusoidalOut, SinusoidalInOut,
        ExponentialIn, ExponentialOut, ExponentialInOut,
        CircularIn, CircularOut, CircularInOut,
        ElasticIn, ElasticOut, ElasticInOut,
        BackIn, BackOut, BackInOut,
        BounceIn, BounceOut, BounceInOut,

    }

    public static class Lerp
    {

        public static LerpControl LerpValue(float startValue, float endValue, float time, EaseType ease = EaseType.Linear)
        {
            return new LerpControl(Lerping, startValue, endValue, time, ease);
        }

        private static (bool, float) Lerping(float startValue, float endValue, float time, float timeElapsed, LerpAction<float> onNext, LerpAction onDone, EaseType ease)
        {
            bool complete = timeElapsed <= time;
            if (complete)
            {
                float lerpValue = Mathf.LerpUnclamped(startValue, endValue, Ease(timeElapsed / time, ease));
                timeElapsed += Time.deltaTime;
                onNext?.Invoke(lerpValue);
            }
            else
            {
                onNext?.Invoke(endValue);
                onDone?.Invoke();
            }
            return (complete, timeElapsed);
        }

        private static float Ease(float value, EaseType easeType)
            => (easeType) switch
            {
                EaseType.Linear => Easing.Linear(value),
                EaseType.QuadraticIn => Easing.Quadratic.In(value),
                EaseType.QuadraticOut => Easing.Quadratic.Out(value),
                EaseType.QuadraticInOut => Easing.Quadratic.InOut(value),
                EaseType.CubicIn => Easing.Cubic.In(value),
                EaseType.CubicOut => Easing.Cubic.Out(value),
                EaseType.CubicInOut => Easing.Cubic.InOut(value),
                EaseType.QuarticIn => Easing.Quartic.In(value),
                EaseType.QuarticOut => Easing.Quartic.Out(value),
                EaseType.QuarticInOut => Easing.Quartic.InOut(value),
                EaseType.QuinticIn => Easing.Quintic.In(value),
                EaseType.QuinticOut => Easing.Quintic.Out(value),
                EaseType.QuinticInOut => Easing.Quintic.InOut(value),
                EaseType.SinusoidalIn => Easing.Sinusoidal.In(value),
                EaseType.SinusoidalOut => Easing.Sinusoidal.Out(value),
                EaseType.SinusoidalInOut => Easing.Sinusoidal.InOut(value),
                EaseType.ExponentialIn => Easing.Exponential.In(value),
                EaseType.ExponentialOut => Easing.Exponential.Out(value),
                EaseType.ExponentialInOut => Easing.Exponential.InOut(value),
                EaseType.CircularIn => Easing.Circular.In(value),
                EaseType.CircularOut => Easing.Circular.Out(value),
                EaseType.CircularInOut => Easing.Circular.InOut(value),
                EaseType.ElasticIn => Easing.Elastic.In(value),
                EaseType.ElasticOut => Easing.Elastic.Out(value),
                EaseType.ElasticInOut => Easing.Elastic.InOut(value),
                EaseType.BackIn => Easing.Back.In(value),
                EaseType.BackOut => Easing.Back.Out(value),
                EaseType.BackInOut => Easing.Back.InOut(value),
                EaseType.BounceIn => Easing.Bounce.In(value),
                EaseType.BounceOut => Easing.Bounce.Out(value),
                EaseType.BounceInOut => Easing.Bounce.InOut(value),
                _ => value,
            };
    }
}
