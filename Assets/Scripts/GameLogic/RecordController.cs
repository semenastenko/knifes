﻿using Knives.UI;
using SaveCore;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Knives.GameLogic
{
    [Serializable]
    public class RecordSave : ISaveable
    {
        public int score;
        public int stage;

        public void UpdateData(object data)
        {
            var obj = data as RecordSave;
            score = obj.score;
            stage = obj.stage;
        }
    }

    public class RecordController : SaveLoad<RecordSave>
    {
        private RecordSave save;
        private CountersContainer _countersContainer;
        public RecordController(UIController UIController)
        {
            _countersContainer = UIController.CountersContainer;
        }

        public void UpdateRecord()
        {

            if (save == null) return;

            var currentScore = _countersContainer.HitCounter.Count;
            var currentStage = _countersContainer.StageCounter.Count;

            _countersContainer.GameOverRecordCounter.UpdateScore(currentScore, currentStage);

            if (currentScore > save.score)
                save.score = currentScore;
            if (currentStage > save.stage)
                save.stage = currentStage;

            UpdateUICounters();
        }

        private void UpdateUICounters()
        {
            _countersContainer.RecordCounter.UpdateScore(save.score, save.stage);
        }

        protected override void OnLoad(RecordSave data)
        {
            if (data != null)
                save = data;
            else
                save = new();

            UpdateUICounters();
        }

        protected override RecordSave OnSave()
        {
            return save;
        }
    }
}
