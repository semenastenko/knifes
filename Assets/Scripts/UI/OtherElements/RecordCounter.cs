using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class RecordCounter : MonoBehaviour, ICounterableElement
    {
        [SerializeField] Text scoreText;
        [SerializeField] Text stageText;

        public void UpdateScore(int score, int stage)
        {
            scoreText.text = $"SCORE {score}";
            stageText.text = $"STAGE {stage}";
        }

        public int Count => throw new System.NotImplementedException();

        public void Clear()
        {
            throw new System.NotImplementedException();
        }

        public void Increment()
        {
            throw new System.NotImplementedException();
        }
    }
}
