﻿using System;
using UnityEngine;

namespace Knives.Settings
{
    [CreateAssetMenu(fileName = "BossDefeatOpenRequest", menuName = "SO/Requests/BossDefeatOpenRequest")]
    public class BossDefeatRequest : RequestSO
    {
        [SerializeField] Request request;

        public static Request CreateRequest() => new Request();

        public override BaseRequest GetRequest() => request;

        [Serializable]
        public class Request : BaseRequest
        {
            public DartboardBoss requaredBoss;

            protected override bool Compare(BaseRequest request)
            {
                if (request is Request)
                {
                    return ((Request)request).requaredBoss == requaredBoss;
                }

                return false;
            }
        }
    }
}