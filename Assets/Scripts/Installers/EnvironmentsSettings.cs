﻿using UnityEngine;
using Knives.Environments;
using Knives.UI;

namespace Knives.Settings
{
    [CreateAssetMenu(fileName = "EnvironmentsSettings", menuName = "SO/EnvironmentsSettings")]
    public class EnvironmentsSettings : ScriptableObject
    {
        [Header("Base prefabs")]
        [SerializeField] Dartboard dartboardPrefab;
        [SerializeField] Knife knifePrefab;
        [SerializeField] Apple applePrefab;
        [SerializeField] Indicator indicatorPrefab;
        [Header("Pool Settings")]
        [SerializeField] int dartboardPoolCount;
        [SerializeField] int knifePoolCount;
        [SerializeField] int applePoolCount;
        [SerializeField] int indicatorPoolCount;
        [Header("Create Settings")]
        [SerializeField] Vector3 dartboardSpawnPosition;
        [SerializeField] Vector3 knifeSpawnPosition;
        [SerializeField, Tooltip("Delay")] float throwDelay = 100;
        [SerializeField] float throwPower = 1000;
        [SerializeField] float dartboardExplosionForce = 100;
        [SerializeField] float dartboardExplosionRadius = 10;
        [SerializeField] float knifeExplosionForce = 100;
        [SerializeField] float appleExplosionForce = 100;
        [Header("Particle Prefabs")]
        [SerializeField] ParticleSystem chipsParticle;
        [SerializeField] ParticleSystem destroyParticle;
        [SerializeField] ParticleSystem crashParticle;
        [SerializeField] ParticleSystem appleParticle;
        [SerializeField] SpriteRenderer blindingEffect;
        [Header("Customization")]
        [SerializeField] DartboardCustomization[] dartboardCustomization;
        [SerializeField] KnifeCustomization[] knifeCustomization;

        public Dartboard DartboardPrefab => dartboardPrefab;
        public Knife KnifePrefab => knifePrefab;
        public Apple ApplePrefab => applePrefab;
        public Indicator IndicatorPrefab => indicatorPrefab;
        public int DartboardPoolCount => dartboardPoolCount;
        public int KnifePoolCount => knifePoolCount;
        public int ApplePoolCount => applePoolCount;
        public int IndicatorPoolCount => indicatorPoolCount;
        public Vector3 DartboardSpawnPosition => dartboardSpawnPosition;
        public Vector3 KnifeSpawnPosition => knifeSpawnPosition;
        public DartboardCustomization[] DartboardCustomization => dartboardCustomization;
        public KnifeCustomization[] KnifeCustomization => knifeCustomization;
        public float ThrowDelay => throwDelay;
        public float ThrowPower => throwPower;
        public float DartboardExplosionForce => dartboardExplosionForce;
        public float DartboardExplosionRadius => dartboardExplosionRadius;
        public float KnifeExplosionForce => knifeExplosionForce;
        public float AppleExplosionForce => appleExplosionForce;

        public ParticleSystem ChipsParticle => chipsParticle;
        public ParticleSystem DestroyParticle => destroyParticle;
        public ParticleSystem CrashParticle => crashParticle;
        public ParticleSystem AppleParticle => appleParticle;
        public SpriteRenderer BlindingEffect => blindingEffect;

    }
}