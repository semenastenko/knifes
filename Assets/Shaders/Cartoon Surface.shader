Shader "HPEW/CartoonShader"
{
	Properties
	{
		_Color("Tint Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal/Bump Map", 2D) = "bump" {}
		_MetallicGlossMap("Metallic map", 2D) = "black" {}

		[Header(Lightning)]
		[Space(10)]
		[HDR] _AmbientColor("Ambient Color", Color) = (0.4,0.4,0.4,1)
		[HDR] _SpecularColor("Specular Color", Color) = (0.9,0.9,0.9,1)
		_Glossiness("Glossiness", Float) = 32
		[HDR] _RimColor("Rim Color", Color) = (1,1,1,1)
		_RimAmount("Rim Amount", Range(0, 1)) = 0.716
		_RimThreshold("Rim Threshold", Range(0, 1)) = 0.1

	}

	SubShader
	{
		Tags { "RenderType" = "Transparent" }

		LOD 200

			Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma surface surf Cel vertex:vert alpha
		#include "UnityCG.cginc"

		#include "Lighting.cginc"
		#include "AutoLight.cginc"

		#pragma target 2.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _MetallicGlossMap;
		float4 _Color;
		float4 _AmbientColor;
		float4 _SpecularColor;
		float4 _RimColor;
		float _Glossiness;
		float _RimAmount;
		float _RimThreshold;

		float4 LightingCel(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			float NdotL = dot(_WorldSpaceLightPos0, s.Normal);

			float3 halfVector = normalize(_WorldSpaceLightPos0 + viewDir);

			float NdotH = dot(s.Normal, halfVector);

			float specularIntensity = pow(NdotH, _Glossiness * _Glossiness);

			float specularIntensitySmooth = smoothstep(0.005, 0.01, specularIntensity);

			float4 specular = specularIntensitySmooth * _SpecularColor;

			float rimDot = (1 - dot(s.Normal, viewDir)) * pow(NdotL, 0.3);
			float fresnelSize = 1 - _RimAmount;

			float4 rim = smoothstep(fresnelSize, fresnelSize * 1.1, rimDot) * _RimColor;

			return float4(s.Albedo.rgb, 1) * (_LightColor0 + _AmbientColor + specular + rim) * _Color;
		}

		struct Input
		{
			float4 pos : SV_POSITION;
			float2 uv_MainTex;
		};

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.pos = UnityObjectToClipPos(v.vertex);
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
			fixed4 col = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = col.rgb;
			o.Alpha = col.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
		}

		ENDCG
	}

	FallBack off
}