using Knives.Settings;
using PolyAndCode.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SaveCore;
using Cysharp.Threading.Tasks;
using System.Linq;

namespace Knives.Environments
{
    [Serializable]
    public class CustomizationSave : ISaveable
    {
        public List<int> openKnives = new();
        public int currentKnife;

        public void UpdateData(object data)
        {
            var obj = data as CustomizationSave;
            openKnives = obj.openKnives;
            currentKnife = obj.currentKnife;
        }
    }


    public class KnivesCustomizationController : SaveLoad<CustomizationSave>, IRecyclableScrollRectDataSource
    {
        private CustomizationSave save;
        private int _dataLength;

        private List<KnifeInfo> _contactList = new List<KnifeInfo>();

        public event Action<KnifeInfo> OnSelectKnife;

        public KnivesCustomizationController(EnvironmentsSettings settings)
        {
            _dataLength = settings.KnifeCustomization.Length;
            InitData(settings.KnifeCustomization);
        }

        private async void InitData(KnifeCustomization[] knifes)
        {
            await UniTask.WaitWhile(() => save == null);

            if (_contactList != null) _contactList.Clear();

            foreach (var knife in knifes)
            {
                var canSelect = false;
                var isOpen = knife.Request == null;

                if (knife.Request == null && save.currentKnife == 0)
                {
                    var id = knife.GetInstanceID();
                    save.openKnives.Add(id);
                    save.currentKnife = id;
                }

                foreach (var openKnife in save.openKnives)
                {
                    var instanceID = knife.GetInstanceID();
                    if (openKnife == instanceID)
                    {
                        if (openKnife == save.currentKnife)
                        {
                            canSelect = true;
                        }
                        isOpen = true;
                    }
                }
                KnifeInfo info = new KnifeInfo(knife, isOpen, OnSelect);
                _contactList.Add(info);

                if (canSelect)
                {
                    info.SetSelect(true);
                }

                if (!isOpen && knife.Request != null)
                {
                    knife.Request.OnReceiveRequest += () => OnReceiveRequest(knife.Request, info);
                    knife.Request.StartReceiveRequest<BossDefeatRequest.Request>();
                }
            }
        }

        private void OnReceiveRequest(BaseRequest request, KnifeInfo knife)
        {
            save.openKnives.Add(knife.Cutomization.GetInstanceID());
            request.Dispose();
            knife.Open();
        }

        private void OnSelect(KnifeInfo knife, bool value)
        {
            if (value)
            {
                foreach (var item in _contactList)
                {
                    if (item != knife)
                    {
                        item.SetSelect(false);
                    }
                    else
                    {
                        save.currentKnife = knife.Cutomization.GetInstanceID();
                        OnSelectKnife?.Invoke(knife);
                    }
                }
            }
        }

        public int GetItemCount()
        {
            return _contactList.Count;
        }

        public IContractCell GetContract(int index)
        {
            return _contactList[index];
        }

        protected override void OnLoad(CustomizationSave data)
        {
            if (data != null)
                save = data;
            else
                save = new();
        }

        protected override CustomizationSave OnSave()
        {
            return save;
        }
    }
}
