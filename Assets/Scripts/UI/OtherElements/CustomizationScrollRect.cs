using PolyAndCode.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Knives.UI
{

    public class CustomizationScrollRect : RecyclableScrollRect, IDisposable
    {
        private IDisposable corutineDispose;

        protected override void Start()
        {

        }

        protected override void InitializeRecyclingSystem()
        {
            corutineDispose = Observable.FromMicroCoroutine(() => _recyclingSystem.InitCoroutine(() =>
                  onValueChanged.AddListener(OnValueChangedListener))).Subscribe();
        }

        public void Dispose()
        {
            corutineDispose?.Dispose();
        }
    }
}
