using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace UICore
{
    public enum PanelState
    {
        undefined,
        closed,
        opening,
        open,
        closing
    }

    public struct PanelEventArgs
    {
        public UIPanel panel;

        public PanelEventArgs(UIPanel panel)
        {
            this.panel = panel;
        }
    }

    public delegate void PanelEventHandler(object sender, PanelEventArgs e);

    public class UIPanel : MonoBehaviour, IUIElement
    {
        private UIPanel parentPanel;
        private List<UIPanel> childPanels;
        private List<UIButton> childButtons;

        public UIPanel Parent => parentPanel;
        public UIPanel[] Childs => childPanels.ToArray();
        public UIButton[] Buttons => childButtons.ToArray();

        public bool IsLock { get; set; }

        public PanelState State { get; private set; }

        public event PanelEventHandler OnBeginOpen;
        public event PanelEventHandler OnFinallyOpen;
        public event PanelEventHandler OnBeginClose;
        public event PanelEventHandler OnFinallyClose;

        void IUIElement.Initialize(IUIElement parent)
        {
            if (childPanels == null)
            {
                childPanels = new List<UIPanel>();
            }
            if (childButtons == null)
            {
                childButtons = new List<UIButton>();
            }

            parentPanel = parent as UIPanel;
            if (parentPanel)
            {
                if (parentPanel.childPanels == null)
                {
                    parentPanel.childPanels = new List<UIPanel>();
                }
                parentPanel.childPanels.Add(this);
            }

            OnAwake();
        }

        protected virtual void OnAwake() { }

        public async UniTask Open(object sender, bool isImmediately = false, bool isImmediatelyParent = false)
        {
            if (State == PanelState.open || State == PanelState.opening || IsLock) return;
            if (Parent) await Parent.Open(sender, isImmediatelyParent || isImmediately, isImmediatelyParent);

            var startPosition = transform.position;

            gameObject.SetActive(true);
            State = PanelState.opening;
            BeginOpen();
            OnBeginOpen?.Invoke(sender, new PanelEventArgs(this));
            if (isImmediately)
                await UniTask.Yield();
            else
                await Opening();
            FinallyOpen();

            transform.position = startPosition;

            State = PanelState.open;
            OnFinallyOpen?.Invoke(sender, new PanelEventArgs(this));
        }

        protected virtual void BeginOpen() { }

        protected virtual async UniTask Opening() => await UniTask.Yield();

        protected virtual void FinallyOpen() { }

        public async UniTask Close(object sender, bool isImmediately = false, bool isImmediatelyParent = false)
        {
            if (State == PanelState.closed || State == PanelState.closing) return;
            List<UniTask> closeChild = new List<UniTask>();
            foreach (var child in Childs)
            {
                closeChild.Add(child.Close(sender, isImmediatelyParent || isImmediately, isImmediatelyParent));
            }
            if (closeChild.Count > 0) await UniTask.WhenAll(closeChild.ToArray());

            var startPosition = transform.position;

            State = PanelState.closing;
            BeginClose();
            OnBeginClose?.Invoke(sender, new PanelEventArgs(this));
            if (isImmediately)
                await UniTask.Yield();
            else
                await Closing();
            FinallyClose();
            gameObject.SetActive(false);

            transform.position = startPosition;

            State = PanelState.closed;
            OnFinallyClose?.Invoke(sender, new PanelEventArgs(this));
        }

        protected virtual void BeginClose() { }

        protected virtual async UniTask Closing() => await UniTask.Yield();

        protected virtual void FinallyClose() { }

        public class Manager : UIElementManager<UIPanel>
        {
            private UIPanel[] rootPanels;

            public UIPanel[] Roots => rootPanels;
            public UIPanel FrontPanel { get; private set; }


            protected override void OnConstruct()
            {
                List<UIPanel> roots = new List<UIPanel>();
                foreach (var panel in AllElements)
                {
                    (panel as IUIElement).Initialize(GetElementInParent<UIPanel>(panel.transform));
                    if (!panel.Parent)
                    {
                        roots.Add(panel);
                    }

                    panel.OnBeginClose += SetParentFront;
                    panel.OnBeginOpen += SetFront;
                }

                rootPanels = roots.ToArray();
                Initialize();
            }

            private async void Initialize()
            {
                List<UniTask> rootTasks = new List<UniTask>();
                foreach (var root in rootPanels)
                {
                    rootTasks.Add(root.Close(this, true, true));
                }

                if (rootTasks.Count > 0) await UniTask.WhenAll(rootTasks.ToArray());

                var startPanel = GetElement<BeginingPanel>(_ => true);
                if (startPanel) _ = startPanel.Open(this, true, true);
            }

            private void SetFront(object sender, PanelEventArgs e) => FrontPanel = e.panel;
            private void SetParentFront(object sender, PanelEventArgs e) => FrontPanel = e.panel.Parent;
        }
    }
}
