﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Knives.UI
{
    public class Indicator : MonoBehaviour
    {
        private Image image;
        private void OnCreated()
        {
            image = GetComponent<Image>();
        }

        public void Select()
        {
            image.color = Color.white;
        }

        private void UnSelect()
        {
            image.color = Color.black;
        }

        public class Pool : MonoMemoryPool<Transform, Indicator>
        {
            private Vector3 localScale;
            protected override void OnCreated(Indicator item)
            {
                base.OnCreated(item);
                item.OnCreated();
                localScale = item.transform.localScale;
            }

            protected override void Reinitialize(Transform parent, Indicator item)
            {
                item.UnSelect();
                item.transform.SetParent(parent);
                item.transform.localScale = localScale;
                (item.transform as RectTransform).anchoredPosition3D = Vector3.zero;
            }

        }
    }
}
