using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using LerpUtility;
using System.Threading;
using Knives.Settings;
using Cysharp.Threading.Tasks;
using Knives.GameLogic;

namespace Knives.Environments
{
    [System.Serializable]
    public struct RotationRule
    {
        public float rotation;
        public float time;

        public RotationRule(float rotation, float time)
        {
            this.rotation = rotation;
            this.time = time;
        }
    }
    public class Dartboard : MonoBehaviour
    {
        private ParticlesController currentParticles;
        private CircleCollider2D _collider;
        private Rigidbody[] rigidbodies;
        private Renderer[] renderers;
        private float originScale;
        private Vector3 originPosition;
        public float Radius => _collider.radius * originScale;

        private bool canRotating;
        private CancellationTokenSource cancellationToken;
        private void Awake()
        {
            rigidbodies = GetComponentsInChildren<Rigidbody>();
            renderers = GetComponentsInChildren<Renderer>();
            _collider = GetComponent<CircleCollider2D>();
        }

        private void Cutomize(Material material)
        {
            foreach (var item in renderers)
            {
                item.material = material;
            }
        }

        public async void StartRules(RotationRule[] rules)
        {
            canRotating = true;
            var ruleIndex = 0;
            cancellationToken = new CancellationTokenSource();
            try
            {
                while (canRotating)
                {
                    var lastIndex = ruleIndex - 1;
                    if (lastIndex < 0) lastIndex = rules.Length - 1;
                    if (ruleIndex >= rules.Length) ruleIndex = 0;

                    await Lerp.LerpValue(rules[lastIndex].rotation, rules[ruleIndex].rotation, rules[ruleIndex].time * 0.25f)
                        .OnNext(x => { Rotating(x); })
                        .Task(cancellationToken);
                    await Lerp.LerpValue(0, 1, rules[ruleIndex].time * 0.75f)
                        .OnNext(x => { Rotating(rules[ruleIndex].rotation); })
                        .Task(cancellationToken);
                    ruleIndex++;
                }
            }
            catch { }
        }

        public async void Animate(float delay)
        {
            await Lerp.LerpValue(transform.localScale.x * 0.1f, transform.localScale.x, delay, EaseType.BackOut)
                .OnNext(s => { transform.localScale = Vector3.one * s; })
                .Task();
        }

        public async void Damage()
        {
            var damageOffset = 0.1f;
            var time = 0.1f;
            var colorIntencity = 2f;

            foreach (var renderer in renderers)
            {
                var color = Color.white * colorIntencity;
                color.a = 1;
                renderer.material.SetColor("_Color", color);
            }
            await Lerp.LerpValue(originPosition.y, originPosition.y + damageOffset, time / 2, EaseType.QuadraticOut)
               .OnNext(y => { transform.position = new Vector3(transform.position.x, y, transform.position.z); })
               .Task();
            await Lerp.LerpValue(originPosition.y + damageOffset, originPosition.y, time / 2, EaseType.QuadraticIn)
              .OnNext(y => { transform.position = new Vector3(transform.position.x, y, transform.position.z); })
              .Task();
            foreach (var renderer in renderers)
            {
                var color = Color.white;
                renderer.material.SetColor("_Color", color);
            }
        }

        public UniTask Fade(float time)
        {
            List<UniTask> tasks = new List<UniTask>();
            foreach (var renderer in renderers)
            {
                var task = Lerp.LerpValue(1, 0, time, EaseType.BackOut)
                        .OnNext(a => { renderer.material.SetColor("_Color", new Color(1, 1, 1, a)); })
                        .Task();
                tasks.Add(task);
            }

            return UniTask.WhenAll(tasks);
        }

        private void Rotating(float rotation)
        {
            if (gameObject != null)
                transform.Rotate(Vector3.forward * rotation * Time.deltaTime);
        }

        public void AddExplosionForce(float explosionForce, Vector2 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode mode = ForceMode.Force)
        {
            currentParticles.InvokeDestroyParticle(transform.position);
            foreach (var rb in rigidbodies)
            {
                rb.transform.SetParent(null);
                rb.gameObject.SetActive(true);
                rb.isKinematic = false;
                rb.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, upwardsModifier, mode);
            }
        }

        public class Pool : MonoMemoryPool<Vector3, Material, Dartboard>
        {
            [Inject] KnivesCustomizationController customizationController;
            [Inject] ParticlesController particles;

            private Transform logParent;

            protected override void Reinitialize(Vector3 position, Material material, Dartboard item)
            {
                item.originPosition = position;
                item.transform.position = position;
                item.transform.localScale = item.originScale * Vector3.one;
                foreach (var rb in item.rigidbodies)
                {
                    rb.transform.SetParent(logParent);
                    rb.isKinematic = true;
                    rb.transform.localPosition = Vector3.zero;
                    rb.transform.localEulerAngles = Vector3.zero;
                }
                item.currentParticles = particles;
                item.Cutomize(material);
            }

            protected override void OnCreated(Dartboard element)
            {
                logParent = element.rigidbodies[0].transform.parent;
                element.originScale = element.transform.localScale.x;
                base.OnCreated(element);
            }

            protected override void OnSpawned(Dartboard element)
            {
                base.OnSpawned(element);
            }

            protected override void OnDespawned(Dartboard element)
            {
                element.canRotating = false;
                element.cancellationToken.Cancel();
                base.OnDespawned(element);
            }
        }
    }
}
