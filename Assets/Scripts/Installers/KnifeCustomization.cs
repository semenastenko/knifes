﻿using UnityEngine;

namespace Knives.Settings
{
    [CreateAssetMenu(fileName = "Knife", menuName = "SO/KnifeCustomization")]
    public class KnifeCustomization : ScriptableObject
    {
        [SerializeField] Sprite knifeSprite;
        [SerializeField] RequestSO request;

        public Sprite KnifeSprite => knifeSprite;
        public BaseRequest Request => request ? request.GetRequest() : null;
    }
}