using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using LerpUtility;
using Cysharp.Threading.Tasks;

namespace Knives.UI
{
    public class SwitchToggle : Toggle
    {
        [SerializeField] Graphic onLable;
        [SerializeField] Graphic offLable;
        [SerializeField] float transitionTime;
        [SerializeField] float transitionScale;
        private Scrollbar scrollbar;
        private GameObject handle;
        private bool? currentTrasition;
        protected override void Awake()
        {
            scrollbar = graphic.GetComponent<Scrollbar>();
            handle = scrollbar.handleRect.GetChild(0).gameObject;
            onValueChanged.AddListener(SwithTransition);
            scrollbar.value = isOn ? 1 : 0;
            SetActiveLables(isOn);
        }

        private async void SwithTransition(bool value)
        {
            if (currentTrasition.HasValue && currentTrasition.Value == value) return;
            await UniTask.WaitWhile(() => currentTrasition.HasValue);
            await TransitionLerp(!value);
        }

        private async UniTask TransitionLerp(bool value)
        {
            currentTrasition = value;

            await TransitionScale(value);
            currentTrasition = null;
        }

        private async UniTask TransitionMovement(bool value)
        {
            await Lerp.LerpValue(value ? 1 : 0, !value ? 1 : 0, transitionTime, EaseType.ExponentialIn)
                .OnNext(x => { scrollbar.value = x; })
                .Task();
        }

        private async UniTask TransitionScale(bool value)
        {
            var scale = handle.transform.localScale.x;

            SetActiveLables(value);

             await UniTask.WhenAll(
                Lerp.LerpValue(handle.transform.localScale.x, scale * (2 - transitionScale), transitionTime/4, EaseType.Linear)
                    .OnNext(x => { handle.transform.localScale = new Vector3(x, handle.transform.localScale.y, scale); })
                    .Task(),
                Lerp.LerpValue(handle.transform.localScale.y, scale * (2 - transitionScale), transitionTime / 4, EaseType.Linear)
                    .OnNext(y => { handle.transform.localScale = new Vector3(handle.transform.localScale.x, y, scale); })
                    .Task()
                    );

            await UniTask.WhenAll(
                TransitionMovement(value),
                Lerp.LerpValue(handle.transform.localScale.y, scale * transitionScale, transitionTime, EaseType.Linear)
                    .OnNext(y => { handle.transform.localScale = new Vector3(handle.transform.localScale.x, y, scale); })
                    .Task());

            SetActiveLables(!value);

            await UniTask.WhenAll(
                Lerp.LerpValue(handle.transform.localScale.x, scale, transitionTime / 4, EaseType.BackOut)
                    .OnNext(x => { handle.transform.localScale = new Vector3(x, handle.transform.localScale.y, scale); })
                    .Task(),
                Lerp.LerpValue(handle.transform.localScale.y, scale, transitionTime / 4, EaseType.BackOut)
                    .OnNext(y => { handle.transform.localScale = new Vector3(handle.transform.localScale.x, y, scale); })
                    .Task()
                    );
        }

        private void SetActiveLables(bool value)
        {
            if (onLable != null)
                onLable.gameObject.SetActive(value);
            if (offLable != null)
                offLable.gameObject.SetActive(!value);
        }
    }
}
