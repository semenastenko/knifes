﻿using Knives.Settings;
using LerpUtility;
using UnityEngine;

namespace Knives.GameLogic
{
    public class ParticlesController
    {
        private EnvironmentsSettings _settings;
        private ParticleSystem chipsParticle;
        private ParticleSystem destroyParticle;
        private ParticleSystem crashParticle;
        private ParticleSystem appleParticle;
        private SpriteRenderer blindingEffect;
        public ParticlesController(EnvironmentsSettings settings)
        {
            _settings = settings;
            InstantiateParticles();
        }

        private void InstantiateParticles()
        {
            chipsParticle = GameObject.Instantiate(_settings.ChipsParticle);
            destroyParticle = GameObject.Instantiate(_settings.DestroyParticle);
            crashParticle = GameObject.Instantiate(_settings.CrashParticle);
            appleParticle = GameObject.Instantiate(_settings.AppleParticle);
            blindingEffect = GameObject.Instantiate(_settings.BlindingEffect);
            blindingEffect.gameObject.SetActive(false);
        }

        public void InvokeChipsParticle(Vector3 position)
        {
            chipsParticle.transform.position = position;

            chipsParticle.Play();
        }

        public void InvokeDestroyParticle(Vector3 position)
        {
            destroyParticle.transform.position = position;

            destroyParticle.Play();
        }

        public void InvokeCrashParticle(Vector3 position)
        {
            crashParticle.transform.position = position;

            crashParticle.Play();
        }

        public void InvokeAppleParticle(Vector3 position)
        {
            appleParticle.transform.position = position;

            appleParticle.Play();
        }

        public async void InvokeBlindingEffect()
        {
            var startAlpha = 0.9f;
            var time = 0.5f;
            blindingEffect.gameObject.SetActive(true);
            blindingEffect.color = new Color(1, 1, 1, startAlpha);
            await Lerp.LerpValue(startAlpha, 0, time)
                .OnNext(a => { blindingEffect.color = new Color(1, 1, 1, a); })
                .Task();
        }
    }
}