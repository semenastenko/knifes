﻿using Cysharp.Threading.Tasks;
using Knives.GameLogic;
using LerpUtility;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class StageCounter : MonoBehaviour, ICounterableElement
    {
        [SerializeField] private Text text;
        [SerializeField] private Image[] images;
        private int count = 0;

        public int Count => count;

        public void SetStage(Stage stage)
        {
            count = stage.Number;
            var index = count % 5;
            if (stage.IsBoss)
            {
                var bossName = "";
                if (stage.Boss != null)
                {
                    bossName = stage.Boss.BossName.ToUpper();
                }
                text.text = $"BOSS " + bossName;
            }
            else
                text.text = $"STAGE {count}";
            for (int i = 0; i < images.Length; i++)
            {
                if (i < index || index == 0)
                    images[i].color = Color.white;
                else
                    images[i].color = Color.black;
            }
            Animate(index - 1);
        }

        public void Increment()
        {
            count++;
            text.text = $"STAGE {count}";
        }

        public void Clear()
        {
            count = 0;
            text.text = $"STAGE {count}";
        }

        private async void Animate(int index)
        {
            if (index < 0) return;
            var startScale = images[index].transform.localScale.x;
            await Lerp.LerpValue(startScale * 1.5f, startScale, 0.3f, EaseType.BackIn)
                .OnNext(s => { images[index].transform.localScale = Vector3.one * s; })
                .Task();
        }

        public async void Shake()
        {
            var offset = 10;
            var rectTransform = transform as RectTransform;
            var startPos = rectTransform.anchoredPosition.x;
            await Lerp.LerpValue(startPos, startPos + offset, 0.15f, EaseType.BackInOut)
                .OnNext(x => { rectTransform.anchoredPosition = new Vector2(x, rectTransform.anchoredPosition.y); })
                .Task();
            await Lerp.LerpValue(startPos + offset, startPos, 0.15f, EaseType.BackOut)
               .OnNext(x => { rectTransform.anchoredPosition = new Vector2(x, rectTransform.anchoredPosition.y); })
               .Task();
        }
    }
}
