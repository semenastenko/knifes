using Cysharp.Threading.Tasks;
using LerpUtility;
using UICore;
using UnityEngine;

namespace Knives.UI
{
    public class MainMenuPanel : BeginingPanel
    {
        protected async override UniTask Opening()
        {
            RectTransform rectTransform = transform as RectTransform;
            await Lerp.LerpValue(Screen.height, rectTransform.anchoredPosition.y, 0.2f, EaseType.Linear)
                .OnNext(y => rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, y))
                .Task();
        }
        protected async override UniTask Closing()
        {
            RectTransform rectTransform = transform as RectTransform;
            await Lerp.LerpValue(rectTransform.anchoredPosition.y, Screen.height, 0.2f, EaseType.QuinticIn)
                .OnNext(y => rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, y))
                .Task();
        }
    }
}
