using Knives.Environments;
using PolyAndCode.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class KnifeCell : MonoBehaviour, ICell
    {
        [SerializeField] Button button;
        [SerializeField] Image image;

        private KnifeInfo _contactInfo;

        private void Start()
        {
            button.onClick.AddListener(ButtonListener);
        }

        public void ConfigureCell(IContractCell contact)
        {
            var contactInfo = contact.GetContract<KnifeInfo>();
            _contactInfo = contactInfo;
            image.sprite = contactInfo.Cutomization.KnifeSprite;
            button.interactable = contactInfo.IsOpen;
            image.color = contactInfo.IsOpen ? Color.white : Color.black;
            _contactInfo.RemoveListeners();
            _contactInfo.OnSelect += OnSelect;
            _contactInfo.OnOpen += OnOpen;

            OnSelect(contactInfo, contactInfo.IsSelect);
        }

        private void OnOpen(KnifeInfo obj)
        {
            button.interactable = true;
            image.color = Color.white;
        }

        private void ButtonListener()
        {
            _contactInfo.SetSelect(true);
        }

        private void OnSelect(KnifeInfo knife, bool value)
        {
            button.targetGraphic.color = value ? Color.green : Color.white;
        }
    }
}
