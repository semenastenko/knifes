using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Knives.UI;
using Cysharp.Threading.Tasks;
using System.Threading;
using System;

namespace Knives.GameLogic
{
    public class GameController
    {
        private long[] vibrationPattern = { 0, 180, 60, 60, 180, 60, 180, 60, 180 };
        private readonly UIController _ui;
        private readonly InputController _input;
        private readonly EnvironmentController _environment;
        private readonly StageController _stage;
        private readonly RecordController _record;
        private readonly CurrencyController _currency;
        private CancellationTokenSource cancellationToken;
        public bool IsGameStarted { get; private set; }

        public GameController(UIController ui, InputController input, EnvironmentController environment, StageController stage, RecordController record, CurrencyController currency, SaveCore.SaveLoadController saveController)
        {
            _ui = ui;
            _input = input;
            _environment = environment;
            _stage = stage;
            _record = record;
            _currency = currency;

            _ui.OnButtonAction += OnButtonAction;
            input.OnTouch += Input_OnTouch;
            _environment.OnAppleSlice += OnAppleSlice;

            _ui.PanelManager.GetElement<GameOverPanel>(x => x is GameOverPanel).OnFinallyOpen += (obj, arg) => saveController.SaveData();
            _ui.PanelManager.GetElement<CustomizationPanel>(x => x is CustomizationPanel).OnFinallyClose += (obj, arg) => saveController.SaveData();
        }

        private void OnAppleSlice()
        {
            _currency.AddApple(1);
        }

        private async void Input_OnTouch()
        {
            if (IsGameStarted)
            {
                try
                {
                    var result = await _environment.ThrowKnife(cancellationToken);
                    if (result == Environments.Knife.ThrowResult.miss)
                    {
                        IsGameStarted = false;
                        if (SettingsController.IsVubration) Vibration.VibratePeek();
                        _ui.CountersContainer.StageCounter.Shake();
                        await UniTask.Delay(1000, cancellationToken: cancellationToken.Token);
                        await _environment.FadeAll(0.1f);
                        OnGameOver();
                    }
                    else if (result == Environments.Knife.ThrowResult.hit)
                    {
                        _ui.CountersContainer.HitCounter.Increment();
                        if (!_ui.CountersContainer.KnivesCounter.SelectNextIndicator())
                        {
                            if (SettingsController.IsVubration) Vibration.Vibrate(vibrationPattern, -1);
                            IsGameStarted = false;
                            _environment.Complete(_stage.Stage);
                            await UniTask.Delay(1500, cancellationToken: cancellationToken.Token);
                            await _environment.FadeAll(0.5f);
                            OnNextStage();
                        }
                        else
                        {
                            if (SettingsController.IsVubration) Vibration.VibratePop();
                            _environment.SpawnKnive();
                        }
                    }
                }
                catch(Exception err) { Debug.Log($"CancellationToken canceled {err.Message}"); }
            }
        }

        private void OnButtonAction(object sender, UICore.ButtonEventArgs e)
        {
            if (e.button is PlayButton && !IsGameStarted)
                StartGame();
        }

        private void StartGame()
        {
            Debug.Log($"Start Game!");
            _stage.NextStage();
            _environment.PlaceEnvironment(_stage.Stage);
            IsGameStarted = true;
            _ui.CountersContainer.KnivesCounter.SetIndicators(_stage.Stage.StageKnivesCount);

            _ui.CountersContainer.StageCounter.SetStage(_stage.Stage);

            cancellationToken = new CancellationTokenSource();
        }

        private async void OnGameOver()
        {
            IsGameStarted = false;
            cancellationToken?.Cancel();
            _environment.Dispose();
            _record.UpdateRecord();
            await _ui.OpenGameOver();
            _stage.Reset();
            _ui.CountersContainer.StageCounter.SetStage(_stage.Stage);
            _ui.CountersContainer.HitCounter.Clear();
        }

        private void OnNextStage()
        {
            cancellationToken?.Cancel();
            IsGameStarted = false;
            _environment.Dispose();
            StartGame();
        }
    }
}
