﻿using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class HitCounter : MonoBehaviour, ICounterableElement
    {
        private Text text;
        private int count = 0;

        public int Count => count;

        private void Awake()
        {
            text = GetComponent<Text>();
        }

        public void Increment()
        {
            count++;
            text.text = count.ToString();
        }

        public void Clear()
        {
            count = 0;
            text.text = count.ToString();
        }
    }
}
