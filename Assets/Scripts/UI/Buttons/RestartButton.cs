using System;
using UICore;

namespace Knives.UI
{
    public class RestartButton : UIButton
    {
        protected override TimeSpan GetDelay() => TimeSpan.FromSeconds(0f);
    }
}
