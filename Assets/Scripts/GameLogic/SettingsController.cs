﻿using Knives.UI;
using SaveCore;
using System;

namespace Knives.GameLogic
{
    [Serializable]
    public class SettingsSave : ISaveable
    {
        public bool isVubration = true;

        public void UpdateData(object data)
        {
            var obj = data as SettingsSave;
            isVubration = obj.isVubration;
        }
    }
    public class SettingsController : SaveLoad<SettingsSave>
    {
        private SettingsSave save;
        private SettingsPanel settingsPanel;

        public static bool IsVubration { get; private set; }

        public SettingsController(UIController UIController)
        {
            settingsPanel = UIController.PanelManager.GetElement<SettingsPanel>(x => true);
            settingsPanel.VibrationSwitch.onValueChanged.AddListener(SetVibration);
        }

        private void SetVibration(bool value)
        {
            save.isVubration = value;
            IsVubration = value;
            if (value)
            {
                Vibration.Vibrate();
            }
        }

        protected override void OnLoad(SettingsSave data)
        {
            if (data != null)
                save = data;
            else
                save = new();

            settingsPanel.VibrationSwitch.isOn = save.isVubration;
            IsVubration = save.isVubration;
        }

        protected override SettingsSave OnSave()
        {
            return save;
        }
    }
}