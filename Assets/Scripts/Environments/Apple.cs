using Knives.GameLogic;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

public class Apple : MonoBehaviour
{
    private ParticlesController particles;
    private Rigidbody2D[] _rigidbodies;
    private Vector3 _slicesPosition;
    private Vector3 _slicesRotation;
    private float forcePower;

    public event Action OnSicle;

    private void OnCreate()
    {
        List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();
        for (int i = 0; i < transform.childCount; i++)
        {
            var rb = transform.GetChild(i).GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rigidbodies.Add(rb);
                _slicesPosition = rb.transform.localPosition;
                _slicesRotation = rb.transform.localEulerAngles;
            }
        }

        _rigidbodies = rigidbodies.ToArray();
        
    }

    private void OnSpawn()
    {
        foreach (var rb in _rigidbodies)
        {
            rb.bodyType = RigidbodyType2D.Static;
            rb.gameObject.SetActive(false);
        }
    }

    private void SliceApple()
    {
        particles.InvokeAppleParticle(transform.position);
        OnSicle?.Invoke();
        var dir = Vector2.left;
        foreach (var rb in _rigidbodies)
        {
            dir = -dir;
            rb.transform.SetParent(null);
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.gameObject.SetActive(true);
            var torque = Random.Range(forcePower, forcePower * 2);
            rb.AddForce((Vector2.down + dir) * forcePower);
            rb.AddTorque(-Mathf.Sign(dir.x) * torque);
        }
        gameObject.SetActive(false);
    }

    private void OnDespawn()
    {
        OnSicle = null;
        foreach (var rb in _rigidbodies)
        {
            if (rb != null)
            {
                rb.bodyType = RigidbodyType2D.Static;
                rb.gameObject.SetActive(false);
                rb.transform.SetParent(transform);
                rb.transform.localEulerAngles = _slicesRotation;
                rb.transform.localPosition = _slicesPosition;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SliceApple();
    }

    public class Pool : MonoMemoryPool<Vector3, float, Apple>
    {
        [Inject] ParticlesController particles;
        private Vector3 startScale;
        private Quaternion startRotation;

        protected override void Reinitialize(Vector3 position, float power, Apple item)
        {
            item.transform.position = position;
            item.transform.localScale = startScale;
            item.transform.localRotation = startRotation;
            item.forcePower = power;
            item.particles = particles;
        }

        protected override void OnCreated(Apple element)
        {
            startScale = element.transform.localScale;
            startRotation = element.transform.localRotation;
            base.OnCreated(element);
            element.OnCreate();
        }

        protected override void OnSpawned(Apple element)
        {
            base.OnSpawned(element);
            element.OnSpawn();
        }

        protected override void OnDespawned(Apple element)
        {
            element.OnDespawn();
            base.OnDespawned(element);
        }
    }
}
