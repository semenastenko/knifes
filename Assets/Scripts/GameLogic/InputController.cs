﻿using System;
using UniRx;
using UnityEngine;

namespace Knives.GameLogic
{
    public class InputController : IDisposable
    {
        private IDisposable updateDispose;

        public event Action OnTouch;
        public InputController()
        {
            updateDispose = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnTouch?.Invoke();
            }
        }

        public void Dispose()
        {
            updateDispose?.Dispose();
        }
    }
}
