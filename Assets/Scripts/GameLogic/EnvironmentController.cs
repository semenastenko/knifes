using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Knives.Environments;
using Knives.Settings;
using Cysharp.Threading.Tasks;
using System;
using Random = UnityEngine.Random;
using System.Threading;

namespace Knives.GameLogic
{
    public class EnvironmentController : IDisposable
    {
        private readonly Dartboard.Pool _dartboardPool;
        private readonly Knife.Pool _knifePool;
        private readonly Apple.Pool _applePool;
        private readonly EnvironmentsSettings _settings;

        private Queue<Dartboard> _spawnedDartboards = new();
        private Queue<Apple> _spawnedApple = new();
        private Stack<Knife> _spawnedKnives = new();
        private List<float> dartboardFreePlace = new();

        public event Action OnAppleSlice;

        public EnvironmentController(Dartboard.Pool dartboardPool, Knife.Pool knifePool, Apple.Pool applePool, EnvironmentsSettings settings)
        {
            _dartboardPool = dartboardPool;
            _knifePool = knifePool;
            _applePool = applePool;
            _settings = settings;
        }

        public void PlaceEnvironment(Stage stage)
        {
            SpawnDartboard(stage);
            SpawnKnive();
        }

        public async UniTask<Knife.ThrowResult> ThrowKnife(CancellationTokenSource cancellationToken)
        {
            var result = await _spawnedKnives.Peek().Throw(cancellationToken);
            return result;
        }

        private void SpawnDartboard(Stage stage)
        {
            var customize = _settings.DartboardCustomization[Random.Range(0, _settings.DartboardCustomization.Length)].DartboardMaterial;
            var spawnKnivesCount = stage.SpawnKnivesCount;
            var canAppleSpawn = stage.CanAppleSpawn;
            var rotationRules = stage.RotationRules;

            if (stage.IsBoss)
            {
                customize = stage.Boss.DartboardMaterial;
                spawnKnivesCount = stage.Boss.KnifeSpawnCount;
                canAppleSpawn = false;
                rotationRules = stage.Boss.RotationRules;
            }

            dartboardFreePlace.Clear();
            for (int i = 0; i < 11; i++)
            {
                dartboardFreePlace.Add(i * 30);
            }
            var dartboard = _dartboardPool.Spawn(_settings.DartboardSpawnPosition, customize);
            _spawnedDartboards.Enqueue(dartboard);


            for (int i = 0; i < spawnKnivesCount; i++)
            {
                var spawnPos = GetCirlePosition(dartboard.Radius);
                var knife = _knifePool.Spawn(spawnPos, _settings.ThrowPower);
                _spawnedKnives.Push(knife);
                SetCircleRotation(knife.gameObject, dartboard.transform);
            }

            var spawnApplePos = GetCirlePosition(dartboard.Radius);

            if (canAppleSpawn)
            {
                var apple = _applePool.Spawn(spawnApplePos, _settings.AppleExplosionForce);
                _spawnedApple.Enqueue(apple);
                SetCircleRotation(apple.gameObject, dartboard.transform);
                apple.OnSicle += () => OnAppleSlice?.Invoke();
            }

            dartboard.StartRules(rotationRules);
            dartboard.Animate(0.5f);
        }

        private Vector3 GetCirlePosition(float radius)
        {
            var randIndex = Random.Range(0, dartboardFreePlace.Count);
            var angle = dartboardFreePlace[randIndex];
            dartboardFreePlace.RemoveAt(randIndex);

            var deltaX = Mathf.Cos(angle) * radius;
            var deltaY = Mathf.Sin(angle) * radius;
            return _settings.DartboardSpawnPosition + new Vector3(deltaX, deltaY);
        }

        private void SetCircleRotation(GameObject item, Transform parent)
        {
            item.transform.SetParent(parent);
            var ang = Vector2.SignedAngle(Vector2.up, item.transform.localPosition);
            item.transform.localEulerAngles = new Vector3(0, 0, ang - 180);
        }

        public void SpawnKnive()
        {
            var knife = _knifePool.Spawn(_settings.KnifeSpawnPosition, _settings.ThrowPower);
            _spawnedKnives.Push(knife);
            knife.Animate(_settings.ThrowDelay);
        }

        public async UniTask FadeAll(float time)
        {
            List<UniTask> tasks = new List<UniTask>();
            foreach (var dartboard in _spawnedDartboards)
            {
                tasks.Add(dartboard.Fade(time));
            }
            foreach (var knife in _spawnedKnives)
            {
                tasks.Add(knife.Fade(time));
            }

            await UniTask.WhenAll(tasks);
        }

        public void Dispose()
        {
            while (_spawnedDartboards.Count > 0)
            {
                var element = _spawnedDartboards.Dequeue();
                if (element != null)
                    _dartboardPool.Despawn(element);
            }

            while (_spawnedKnives.Count > 0)
            {
                var element = _spawnedKnives.Pop();
                if (element != null)
                    _knifePool.Despawn(element);
            }

            while (_spawnedApple.Count > 0)
            {
                var element = _spawnedApple.Dequeue();
                if (element != null)
                    _applePool.Despawn(element);
            }
        }

        public void Complete(Stage stage)
        {
            if (stage.IsBoss)
            {
                var request = BossDefeatRequest.CreateRequest();
                request.requaredBoss = stage.Boss;
                BaseRequest.Publish(request);
            }
            Explosion();
        }

        private void Explosion()
        {
            float radius = 0;
            Vector3 explosionPosition = _settings.DartboardSpawnPosition;
            foreach (var dartboard in _spawnedDartboards)
            {
                radius = dartboard.Radius;
            }
            explosionPosition.y -= radius;

            foreach (var knife in _spawnedKnives)
            {
                knife.AddExplosionForce(_settings.KnifeExplosionForce);
            }

            foreach (var dartboard in _spawnedDartboards)
            {
                dartboard.AddExplosionForce(_settings.DartboardExplosionForce, explosionPosition, radius * _settings.DartboardExplosionRadius);
                dartboard.gameObject.SetActive(false);
            }

        }
    }
}