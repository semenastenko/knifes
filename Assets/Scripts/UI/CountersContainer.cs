﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Knives.UI
{
    public class CountersContainer
    {
        private List<ICounterableElement> elements = new();

        public readonly KinvesCounter KnivesCounter;
        public readonly HitCounter HitCounter;
        public readonly AppleCounter AppleCounter;
        public readonly StageCounter StageCounter;
        public readonly RecordCounter RecordCounter;
        public readonly GameoverRecordCounter GameOverRecordCounter;

        [Inject]
        public CountersContainer(ICounterableElement[] counterables)
        {
            elements.AddRange(counterables);

            KnivesCounter = GetElement<KinvesCounter>();
            HitCounter = GetElement<HitCounter>();
            AppleCounter = GetElement<AppleCounter>();
            AppleCounter = GetElement<AppleCounter>();
            StageCounter = GetElement<StageCounter>();
            RecordCounter = GetElement<RecordCounter>();
            GameOverRecordCounter = GetElement<GameoverRecordCounter>();
        }

        public T GetElement<T>() where T: class, ICounterableElement
            => elements.Find(x => x is T) as T;

        public T[] GetAllElement<T>() where T : class, ICounterableElement
            => Array.ConvertAll(elements.FindAll(x => x is T).ToArray(), item => (T)item);
    }
}
