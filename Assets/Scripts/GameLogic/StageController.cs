﻿using Knives.Environments;
using Knives.Settings;
using UnityEngine;
using Utility;

namespace Knives.GameLogic
{
    public class StageController
    {
        readonly GameSettings _settings;

        private WeightedRandom<int> _spawnKivesCountRandom;
        private WeightedRandom<int> _stageKivesCountRandom;
        private WeightedRandom<RotationRule[]> _rotationRulesRandom;

        public Stage Stage { get; } = new Stage();

        public StageController(GameSettings settings)
        {
            _settings = settings;
            _spawnKivesCountRandom = new WeightedRandom<int>();
            _stageKivesCountRandom = new WeightedRandom<int>();
            _rotationRulesRandom = new WeightedRandom<RotationRule[]>();

            for (int i = 0; i < _settings.KnifeSpawnRandomWeight.Length; i++)
            {
                _spawnKivesCountRandom.AddEntry(i + 1, _settings.KnifeSpawnRandomWeight[i]);
            }

            foreach (var item in _settings.KnifeStageRandomWeight)
            {
                _stageKivesCountRandom.AddEntry(item.key, item.value);
            }

            foreach (var item in _settings.RotationRules)
            {
                _rotationRulesRandom.AddEntry(item.key, item.value);
            }
        }

        public void NextStage()
        {
            var spawnKnivesCount = 0;
            var stageKivesCount = _settings.KnifeStageRandomWeight[0].key;
            var rotatuinRules = _settings.RotationRules[0].key;
            DartboardBoss boss = null;
            if (Stage.Number > 0)
            {
                spawnKnivesCount = _spawnKivesCountRandom.GetRandom();
                stageKivesCount = _stageKivesCountRandom.GetRandom();
                rotatuinRules = _rotationRulesRandom.GetRandom();
            }

            if (Stage.IsNextBoss)
            {
                boss = _settings.Bosses[Random.Range(0, _settings.Bosses.Length)];
                stageKivesCount = boss.StageKivesCount;
            }

            var canAppleSpawn = Random.Range(0, 100) <= _settings.AppleSpawnChance;

            Stage.Next(spawnKnivesCount, stageKivesCount, canAppleSpawn, rotatuinRules, boss);
        }

        public void Reset()
        {
            Stage.Reset();
        }
    }
}
