﻿using Knives.Environments;
using System.Collections.Generic;
using UnityEngine;

namespace Knives.Settings
{
    [System.Serializable]
    public struct KeyValue<Key, Value>
    {
        public Key key;  
        public Value value;
    }

    [CreateAssetMenu(fileName = "GameSettings", menuName = "SO/GameSettings")]
    public class GameSettings : ScriptableObject
    {
        [SerializeField, Range(0, 100)]
        float appleSpawnChance = 25;
        [SerializeField, Tooltip("Шанс спавна нескольких ножей. Количество элементов в массиве - максимальное количество ножей.")] 
        float[] knifeSpawnRandomWeight; 
        [SerializeField, Tooltip("Количетво ножей для бросания.")]
        KeyValue<int, float>[] knifeStageRandomWeight;
        [SerializeField] DartboardBoss[] bosses;
        [SerializeField] KeyValue<RotationRule[], float>[] rotationRules;

        public float AppleSpawnChance => appleSpawnChance;
        public float[] KnifeSpawnRandomWeight => knifeSpawnRandomWeight;
        public KeyValue<int, float>[] KnifeStageRandomWeight => knifeStageRandomWeight;
        public DartboardBoss[] Bosses => bosses;
        public KeyValue<RotationRule[], float>[] RotationRules => rotationRules;
    }
}