﻿using Cysharp.Threading.Tasks;
using UICore;
using LerpUtility;
using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class SettingsPanel : UIPanel
    {
        private Image bgImage;

        [SerializeField] SwitchToggle vibrationSwitch;

        public SwitchToggle VibrationSwitch => vibrationSwitch;

        protected override void OnAwake()
        {
            bgImage = GetComponent<Image>();
        }
        protected async override UniTask Opening()
        {
            await Lerp.LerpValue(0, 0.8f, 0.2f, EaseType.Linear)
                .OnNext(a => bgImage.color = new Color(bgImage.color.r, bgImage.color.g, bgImage.color.b, a))
                .Task();
        }

        protected async override UniTask Closing()
        {
            await Lerp.LerpValue(0.8f, 0, 0.2f, EaseType.Linear)
                .OnNext(a => bgImage.color = new Color(bgImage.color.r, bgImage.color.g, bgImage.color.b, a))
                .Task();
        }
    }
}
