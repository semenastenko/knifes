﻿using Knives.UI;
using SaveCore;
using System;
using UnityEngine;

namespace Knives.GameLogic
{
    [Serializable]
    public class CurrencySave : ISaveable
    {
        public int appleCurrency;

        public void UpdateData(object data)
        {
            var obj = data as CurrencySave;
            appleCurrency = obj.appleCurrency;
        }
    }

    public class CurrencyController : SaveLoad<CurrencySave>
    {
        private CurrencySave save;
        private CountersContainer _countersContainer;

        public CurrencyController(UIController UIController)
        {
            _countersContainer = UIController.CountersContainer;
        }

        public bool TrySpendApple(int value)
        {
            if (save != null && save.appleCurrency - value >= 0)
            {
                save.appleCurrency -= value;
                UpdateCurrencyCounters();
                return true;
            }
            return false;
        }

        public void AddApple(int value)
        {
            save.appleCurrency += value;
            UpdateCurrencyCounters();
        }

        private void UpdateCurrencyCounters()
        {
            _countersContainer.AppleCounter.SetCount(save.appleCurrency);
        }

        protected override void OnLoad(CurrencySave data)
        {
            if (data != null)
                save = data;
            else
                save = new();
            UpdateCurrencyCounters();
        }

        protected override CurrencySave OnSave()
        {
            return save;
        }
    }
}
