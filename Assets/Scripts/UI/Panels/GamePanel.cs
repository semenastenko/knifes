using Cysharp.Threading.Tasks;
using LerpUtility;
using UICore;
using UnityEngine;
using UnityEngine.UI;

namespace Knives.UI
{
    public class GamePanel : UIPanel
    {
        private Image bgImage; 
        protected override void OnAwake()
        {
            bgImage = GetComponent<Image>();
        }
        protected async override UniTask Opening()
        {
            await Lerp.LerpValue(0, 1, 0.2f, EaseType.Linear)
                .OnNext(a => bgImage.color = new Color(bgImage.color.r, bgImage.color.g, bgImage.color.b, a))
                .Task();
        }

        protected async override UniTask Closing()
        {
            await Lerp.LerpValue(1, 0, 0.2f, EaseType.Linear)
                .OnNext(a => bgImage.color = new Color(bgImage.color.r, bgImage.color.g, bgImage.color.b, a))
                .Task();
        }
    }
}
