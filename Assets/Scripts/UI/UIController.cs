﻿using System.Collections.Generic;
using UnityEngine;
using UICore;
using Cysharp.Threading.Tasks;
using UnityEngine.UI;

namespace Knives.UI
{
    public class UIController : UIContainer
    {
        private List<(string, bool)> bindLog = new List<(string, bool)>();
        public CountersContainer CountersContainer { get; private set; }

        public UIController(ICounterableElement[] counterables)
            : base()
        {
            CountersContainer = new CountersContainer(counterables);
        }

        protected override void BindElements()
        {
            //Bind PlayButton in MainMenu
            bindLog.Add(BindQueue<PlayButton>(x => x.RootPanel is MainMenuPanel,
                ButtonAction,
                _ => UniTask.WhenAll(
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.closed),
                    PanelAction<GamePanel>(x => x is GamePanel, PanelState.open))));

            //Bind RestartButton in GameOver
            bindLog.Add(BindQueue<PlayButton>(x => x.RootPanel is GameOverPanel,
                _ => PanelAction<GameOverPanel>(x => x is GameOverPanel, PanelState.closed),
                _ => PanelAction<GamePanel>(x => x is GamePanel, PanelState.open),
                ButtonAction
                ));

            //Bind QuitButton in GameOver
            bindLog.Add(BindQueue<BackButton>(x => x.RootPanel is GameOverPanel,
                ButtonAction,
                _ => UniTask.WhenAll(
                    PanelAction<GameOverPanel>(x => x is GameOverPanel, PanelState.closed),
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open))
                ));

            //Bind SettingsButton in MainMenu
            bindLog.Add(BindQueue<SettingsButton>(x => x.RootPanel is MainMenuPanel,
                _ => UniTask.WhenAll(
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.closed, true),
                    PanelAction<SettingsPanel>(x => x is SettingsPanel, PanelState.open))));

            //Bind QuitButton in SettingsPanel
            bindLog.Add(BindQueue<BackButton>(x => x.RootPanel is SettingsPanel,
                _ => UniTask.WhenAll(
                    PanelAction<SettingsPanel>(x => x is SettingsPanel, PanelState.closed),
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open, true))
                ));

            //Bind CustomizationButton in MainMenu
            bindLog.Add(BindQueue<CustomizationButton>(x => x.RootPanel is MainMenuPanel,
                _ => UniTask.WhenAll(
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.closed, true),
                    PanelAction<CustomizationPanel>(x => x is CustomizationPanel, PanelState.open))));

            //Bind QuitButton in CustomizationPanel
            bindLog.Add(BindQueue<BackButton>(x => x.RootPanel is CustomizationPanel,
                _ => UniTask.WhenAll(
                    PanelAction<CustomizationPanel>(x => x is CustomizationPanel, PanelState.closed),
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open, true))
                ));
        }

        protected async override void OnEscapeKeyDown()
        {
            if (PanelManager.FrontPanel is GamePanel)
            {

            }
            else if (PanelManager.FrontPanel is GameOverPanel)
            {
                await ButtonAction(ButtonManager.GetElement<BackButton>(x => x.RootPanel is GameOverPanel));
                await UniTask.WhenAll(
                    PanelAction<GameOverPanel>(x => x is GameOverPanel, PanelState.closed),
                    PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open));
            }
        }

        public async UniTask OpenGameOver()
        {
            await PanelAction<GamePanel>(x => x is GamePanel, PanelState.closed);
            await PanelAction<GameOverPanel>(x => x is GameOverPanel, PanelState.open);
        }
    }
}
