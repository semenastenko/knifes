﻿using System;
using UnityEngine;
using UniRx;

namespace Knives.Settings
{
    public abstract class RequestSO : ScriptableObject
    {
        public abstract BaseRequest GetRequest();
    }

    public abstract class BaseRequest : IDisposable
    {
        public event Action OnReceiveRequest;
        IDisposable disposable;
        public static void Publish<T>(T request) where T : BaseRequest
        {
            MessageBroker.Default.Publish<BaseRequest>(request);
        }

        public void StartReceiveRequest<T>() where T : BaseRequest
        {
            disposable?.Dispose();
            disposable = MessageBroker.Default.Receive<BaseRequest>().Subscribe(message =>
            {
                if (message is T && Compare(message))
                {
                    OnReceiveRequest?.Invoke();
                }
            });
        }

        protected abstract bool Compare(BaseRequest request);

        public void Dispose()
        {
            disposable?.Dispose();
        }
    }
}